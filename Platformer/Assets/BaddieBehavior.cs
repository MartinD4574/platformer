﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaddieBehavior : MonoBehaviour {

    // two GameObjects used to determine where this thing is walking
    public GameObject currentPoint;
    public GameObject otherPoint;
    public float length = 3.0f;
    public float progress = 0.0f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector2.Lerp(currentPoint.transform.position,otherPoint.transform.position,progress/length);
        progress += Time.deltaTime;
        if (progress >= length) {
            SwapPositions();
        }
		
	}
    /*
    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player"){
            gameObject.SetActive(false);
        }
    }*/

    void SwapPositions() {
        Vector3 tempPosition = currentPoint.transform.position;
        currentPoint.transform.position = otherPoint.transform.position;
        otherPoint.transform.position = tempPosition;
        progress = 0.0f;

        //make sure the sprite faces the right way
        //need to activate and deactivate the correct eyes
        if (currentPoint.transform.position.x>otherPoint.transform.position.x)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
        }
    }
}
