using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseSystemWithAudio : MonoBehaviour {

    public GameObject PauseMenu;
    [SerializeField] AudioSource mainAudioSource1;
    [SerializeField] private AudioSource secondarySource;
    private bool menuOpened = false;
   
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        hidePaused();
    }

    // Update is called once per frame
    void Update()
    {
        //When key is pressed
        if (Input.GetKeyDown(KeyCode.B) && PauseMenu.activeSelf == false)
        {
            Time.timeScale = 0;
            mainAudioSource1.Pause();
            secondarySource.Pause();
            menuOpened = true;
            showPaused();
        }
        else if (Input.GetKeyDown(KeyCode.B) && PauseMenu.activeSelf == true)
        {
            Time.timeScale = 1;
            mainAudioSource1.Play();
            secondarySource.Play();
            menuOpened = false;
            hidePaused();
        }


    }

    public void pauseControl()
    {
        if (PauseMenu.activeSelf == false)
        {
            Time.timeScale = 0;
			mainAudioSource1.Pause();
			secondarySource.Pause();
			menuOpened = true;
            showPaused();
        }
        else if (PauseMenu.activeSelf == true)
        {
            Time.timeScale = 1;
			mainAudioSource1.Play();
			secondarySource.Play();
			menuOpened = false;
            hidePaused();
        }
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
       
        PauseMenu.SetActive(true);
        
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        
         PauseMenu.SetActive(false);
        
        
    }

    public void LoadByIndex(int sceneIndex)
    {
        SceneManager.LoadScene(0);
        //pauseObjects = GameObject.FindGameObjectsWithTag("Paused");
    }


    public bool isPaused()
    {
        return menuOpened;
    }
}
