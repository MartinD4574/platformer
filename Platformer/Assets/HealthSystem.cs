﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class HealthSystem : MonoBehaviour {

    public GameObject player;
    public int maxHealth = 3;
    public int currentHealth = 3;
    private float timer = 0.0f;
    private bool mercy = false; // While true the player cannot lose life

    //array to store and update ui health sprites
    public GameObject[] healthSprites = new GameObject[3];
    // reference to UI text
    public Text lifeText;
    public Text gameOverText;

   //private Scene loadedLevel = SceneManager.GetActiveScene();

   

    // Use this for initialization
    void Start () {
        gameOverText.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        lifeText.text = "x" + PlayerLives.Lives;
  
        
        if (timer > 0.0)
        {
            timer -= Time.deltaTime;
        }
        else {
            mercy = false;
        }

        //test buttons to be removed later
        if (Input.GetKey(KeyCode.O))
        {
            LoseHealth();
        }

        // Has the player lose a life if they run out of health
        if (currentHealth <= 0) {
            LoseLife();
        }
    }


    //Functions to increase and decrease the player's health
    public void GainHealth()
    {
        //Prevents the player from exceeding the maximum amount of health
        if(currentHealth < maxHealth)
        {
            healthSprites[currentHealth].SetActive(true);
            currentHealth++;
        }
    }

    //decrements health and updates UI. 
    //also contains mercy invincibility timer
    public void LoseHealth()
    {
        if (mercy == false)
        {
            //Prevents the player from going into negative levels of health
            if (currentHealth > 0)
            {
                currentHealth--;
                healthSprites[currentHealth].SetActive(false); // update UI for current health value
                timer = 1.0f;
                mercy = true; // Make the player temporarily immune to damage
            }
        }
    }

    //resets health to maximum
    public void ResetHealth() {
        if (currentHealth < maxHealth)
        {
            currentHealth = maxHealth;

            for (int i = 0; i < maxHealth; i++)
            {
                healthSprites[i].SetActive(true);// update UI for current health value
            }

        }
    }

    //resets the scene and lowers player's life when they run out of health
    public void LoseLife() {
        if (PlayerLives.Lives > 0)
        {
            PlayerLives.Lives--;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else {
            gameOverText.gameObject.SetActive(true);
            Invoke("MainMenu", 5);
        }
    }

    //increments the player's number of lives
    public void GainLife() {
        PlayerLives.Lives++;
    }

    //resets number of lives to 3
    public void ResetLives() {
        PlayerLives.Lives = 3;
    }

    //return to main menu
    public void MainMenu() {
        SceneManager.LoadScene(0);
    }
}
