﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarioDeath : MonoBehaviour
{

	[SerializeField] private AudioSource PlayerAudioSource;
	[SerializeField] private AudioClip deathSceneSound;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (PlayerAudioSource.isPlaying) 
			PlayerAudioSource.Stop();
		
		
		PlayerAudioSource.clip = deathSceneSound;
		PlayerAudioSource.Play();
	}
}
