﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishMusic : MonoBehaviour
{

	[SerializeField] private AudioSource audioSource;
	[SerializeField] private AudioClip audioClip;
	
	
	// Use this for initialization
	void Start ()
	{
		audioSource.clip = audioClip;
		audioSource.Play();
		audioSource.loop = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
