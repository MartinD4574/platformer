﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[Header("Physics")]
	public Rigidbody2D playerRigidBody;
	public float moveForce;
	public float airForce;
	public float maxSpeed;
	private float originalMaxSpeed;
	public float sprintMaxSpeed;
	private float finalSpeed;
	public float jumpForce;

	[Header("Ground")]
	public bool grounded;
	public Transform groundCheck;
	public bool jumping;

	bool isMovingLeft = true;
	bool isMovingRight = true;
	bool isSprinting = true;

	bool isFacingRight;

	private SpriteRenderer spriteRender;

	// Use this for initialization
	void Start () {
		originalMaxSpeed = maxSpeed;
		spriteRender = this.GetComponent<SpriteRenderer> ();
		isFacingRight = true;
		jumping = false;
	}

	void FixedUpdate () {
		grounded = Physics2D.Linecast (transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		//Debug.Log ("Grounded: " + grounded);

		if (Input.GetKeyDown (KeyCode.Space) && grounded) {
			playerRigidBody.AddForce (new Vector2 (0f, jumpForce));
			jumping = true;
		}

		if (playerRigidBody.velocity.y < 0) {
			jumping = false;
		}

		/*
		if (isMovingRight && playerRigidBody.velocity.x < maxSpeed) {
			playerRigidBody.AddForce (Vector2.right * moveForce);
		} else if (isMovingLeft && playerRigidBody.velocity.x < maxSpeed) {
			playerRigidBody.AddForce (-Vector2.right * moveForce);
		}

		if (isSprinting) {
			maxSpeed = sprintMaxSpeed;
		} else {
			maxSpeed = originalMaxSpeed;
		}

		if (Mathf.Abs (playerRigidBody.velocity.x) > maxSpeed && grounded) {
			playerRigidBody.velocity = new Vector2 (Mathf.Sign (playerRigidBody.velocity.x) * maxSpeed, playerRigidBody.velocity.y);
		}
		*/

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.A)) {
			isMovingLeft = true;
			isMovingRight = false;

			isFacingRight = false;
		} else if (Input.GetKey (KeyCode.D)) {
			isMovingLeft = false;
			isMovingRight = true;

			isFacingRight = true;
		} else {
			isMovingLeft = false;
			isMovingRight = false;
		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			isSprinting = true;
		} else {
			isSprinting = false;
		}

		if (isSprinting && grounded) {
			finalSpeed = sprintMaxSpeed;
		} else if (!isSprinting && grounded) {
			finalSpeed = originalMaxSpeed;
		}

		if (isMovingRight) {
			transform.Translate (Vector3.right * Time.deltaTime * finalSpeed);
		} else if (isMovingLeft) {
			transform.Translate (Vector3.left * Time.deltaTime * finalSpeed);
		}

		if (isFacingRight) {
			spriteRender.flipX = false;
		} else {
			spriteRender.flipX = true;
		}
	}

	public bool getJumping(){
		return jumping;
	}

	public bool getGrounded(){
		return grounded;
	}
}
