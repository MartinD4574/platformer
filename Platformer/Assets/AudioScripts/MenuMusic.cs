﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMusic : MonoBehaviour
{
	[SerializeField] private AudioSource mainSource;
	[SerializeField] private AudioClip music; 

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update ()
	{
		if (mainSource.isPlaying)
			mainSource.Stop();
		mainSource.clip = music;
		mainSource.Play();
	}
}
