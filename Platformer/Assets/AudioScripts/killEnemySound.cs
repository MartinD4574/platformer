﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillEnemySound : MonoBehaviour
{
	[SerializeField] private AudioSource secondaryFx;
	[SerializeField] private AudioClip clip;
	
	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update ()
	{
		if (secondaryFx.isPlaying)
		{
			secondaryFx.Stop();
		}
		secondaryFx.clip = clip;
		secondaryFx.Play();
	}
}
