﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;


public class JumpSound : MonoBehaviour
{
	[SerializeField] private AudioSource PlayerSoundEffect;
	[SerializeField] private AudioClip jumpSound;
	public PlayerController player;
	private bool onGround = true;
	

	
	// Use this for initialization
	void Start ()
	{
		
		PlayerSoundEffect.volume = .1f;
		
		
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		onGround = player.grounded;
		
        if (Input.GetKeyDown(KeyCode.Space) && onGround)
		{
			
			PlayerSoundEffect.clip = jumpSound;
			PlayerSoundEffect.Play();
			
		}
		
	}

	

}
