﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaceBehavior : MonoBehaviour {
    [Header("Ground")]
    public bool grounded;
    public Transform groundCheck;

    public float speed=3.5f;
    public bool falling = false;
    public bool landed = false;

    public float startingHeight;
	// Use this for initialization
	void Start () {
        startingHeight = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
        if (falling) {
            transform.Translate(0, -Time.deltaTime * speed, 0);
        }
        if (grounded) {
            falling = false;
            landed = true;
        }
        if (landed) {
            transform.Translate(0, Time.deltaTime * speed, 0);
        }
        if (transform.position.y >= startingHeight) {
            landed = false;
        }
	}

    void FixedUpdate() {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag=="Player") {
            falling = true;
        }
    }
}
